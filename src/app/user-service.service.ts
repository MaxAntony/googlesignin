import { Injectable } from '@angular/core';
import { SocialUser } from 'angularx-social-login';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {
  private _user: BehaviorSubject<SocialUser>;

  constructor() {
    const jsonuser = localStorage.getItem('userKey');
    const user = JSON.parse(jsonuser);
    // forma corta
    // this._user = new BehaviorSubject<SocialUser>(user ? user : null);
    if (user) {
      this._user = new BehaviorSubject<SocialUser>(user);
    } else {
      this._user = new BehaviorSubject<SocialUser>(null);
    }
  }
  public getUser(): Observable<SocialUser> {
    return this._user.asObservable();
  }
  public setUser(user: SocialUser): void {
    const jsonUser = JSON.stringify(user);
    localStorage.setItem('userKey', jsonUser);
    this._user.next(user);
  }
}
