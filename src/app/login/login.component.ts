import { Component, OnInit } from '@angular/core';

// terceros
import { AuthService, SocialUser } from 'angularx-social-login';
import {
  GoogleLoginProvider,
  FacebookLoginProvider
} from 'angularx-social-login';
import { UserServiceService } from '../user-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  usuario: SocialUser;
  logueado = false;

  constructor(
    private authService: AuthService,
    private userSubject: UserServiceService,
    private router: Router
  ) {}

  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }
  signInWithFB(): void {
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
  }
  signOut(): void {
    this.authService.signOut();
  }

  irDashboard() {
    this.router.navigate(['./admin']);
  }

  ngOnInit() {
    this.authService.authState.subscribe(user => {
      if (user) {
        this.logueado = true;
        this.usuario = user;
        this.irDashboard();
      } else {
        this.logueado = false;
        console.log('logueo salio mal');
      }
      this.userSubject.setUser(user);
      console.log(user);

      console.log(this.usuario);
    });
  }
}
